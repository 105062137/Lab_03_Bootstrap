# Software Studio 2018 Spring Lab03 Bootstrap and RWD
![Bootstrap logo](./Bootstrap logo.jpg)
## Grading Policy
* **Deadline: 2018/03/20 17:20 (commit time)**

## Todo
1. Check your username is Student ID
2. **Fork this repo to your account, remove fork relationship and change project visibility to public**
3. Make a personal webpage that has RWD
4. Use bootstrap from [CDN](https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css)
5. Use any template form the inernet but you should provide the URL in your project README.md
6. Use at least **Ten** bootstrap elements by yourself which is not include the template
7. You web page template should totally fit to your original personal web page content
8. Modify your markdown properly (check all todo and fill all item)
9. Deploy to your pages (https://[username].gitlab.io/Lab_03_Bootstrap)
10. Go home!

## Student ID , Name and Template URL
- [x] Student ID : 105062137
- [x] Name :李宗穎
- [x] URL : https://getbootstrap.com/docs/4.0/examples/blog/

## 10 Bootstrap elements (list in here)
1. container 框框
2. col-4 表格
3. p-2 間隔
4. text-muted 字的顏色
5. lead 字體大小
6. align-items-start 對齊
7. font-italic 斜體
8. list-unstyled 清單
9. img-thumbnail 調整圖片
10. progress-bar 進度條
